package com.hostel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hostel.entity.Order;

public interface OrderDao extends JpaRepository<Order, String>{
	//
	@Modifying
	@Query(value = "update x_order set status = 'complete' where id = ? ", nativeQuery = true) 
	public void complete(String id);
	
	@Modifying
	@Query(value = "update x_order set status = 'canceled' where id = ? ", nativeQuery = true) 
	public void cancel(String id);
	
	@Query(value = "select * from x_order where user_id = ?", nativeQuery = true) 
	public List<Order> all(String user_id);

}
