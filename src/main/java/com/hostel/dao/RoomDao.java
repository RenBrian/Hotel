package com.hostel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hostel.entity.Room;

public interface RoomDao extends JpaRepository<Room, String>{
	
	@Modifying
	@Query(value = "update x_room set status = 1 where id = ? ", nativeQuery = true) 
	public void checkin(String id);
	
	@Modifying
	@Query(value = "update x_room set status = 0 where id = ? ", nativeQuery = true) 
	public void checkout(String id);
	
	@Query(value = "select * from x_room where hotel_id = ? ", nativeQuery = true)
	public List<Room> findByHotelID(String hotel_id);

}
