package com.hostel.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hostel.entity.User;

public interface UserDao extends JpaRepository<User, String>{
	//通过Email查找
	@Query(value = "select * from x_user where email = ?", nativeQuery = true)
	public User findByEmail(String email);
	
	@Modifying
	@Query(value = "update x_user set password=? where id=?", nativeQuery = true)
	public void updatePassword(String password, String userid);
	
	@Modifying
	@Query(value = "update x_user set balance=? where id=?", nativeQuery = true)
	public void updateBalance(double balance, String userid);
	
	@Modifying
	@Query(value = "update x_user set identity=?, tele=?, name=? where id=?", nativeQuery = true)
	public void updateInfo(String identity, String tele, String name, String id);
}
