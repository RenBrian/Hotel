package com.hostel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.hostel.entity.Hotel;

public interface HotelDao extends JpaRepository<Hotel, String>{
	
	//查找某个种类
	@Query(value = "select count(id) from x_hotel where type = ?", nativeQuery = true)
	public long countTagType(String tag_type);
	@Query(value = "select * from x_hotel where type = ? order by date desc limit ?, ?", nativeQuery = true)
	public List<Hotel> findByTagType(String tag_type, int offset, int rows);
	
	//模糊查找项目
	@Query(value = "select count(id) from x_hotel where name like ?", nativeQuery = true)
	public long countKey(String project_name);
	@Query(value = "select * from x_hotel where name like ? order by date desc limit ?, ?", nativeQuery = true)
	public List<Hotel> findByNameLike(String project_name, int offset, int rows);
	
	//查找所有项目
	@Query(value = "select * from x_hotel order by date desc limit ?, ?", nativeQuery = true)
	public List<Hotel> findAllProjects(int offset, int rows);
	
	@Modifying
	@Query(value = "update x_package set ismarked = 1 where id = ? ", nativeQuery = true) 
	public void mark(String id);

}
