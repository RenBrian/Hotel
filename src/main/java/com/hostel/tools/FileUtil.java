package com.hostel.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @ClassName: FileUtil  
 * @Description: 文件相关 - 写入文件 - 读取文件目录 
 * @author Brian  
 * @date 2018年6月22日  
 *
 */
public class FileUtil {
	//上传时候写入文件
	public static void uploadFile(byte[] file, String filePath, String fileName) {
		File targetFile = new File(filePath);
		if(!targetFile.exists()) {
			targetFile.mkdirs();
		}
		try {
			FileOutputStream out = new FileOutputStream(filePath + fileName);
			out.write(file);
			out.flush();
			out.close();
//			System.out.println(targetFile.getAbsolutePath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	//读取文件地址
	public static ArrayList<String> getImagesPath(String imgFolder) throws FileNotFoundException {
		ArrayList<String> imagePaths = new ArrayList<String>();
		File file = new File(imgFolder);
		File[] images = file.listFiles();
		for (int i = 0; i < images.length; i++) {
//			System.out.println(images[i].getName());
			imagePaths.add(imgFolder + "/" + images[i].getName());
		}
		System.out.println(imagePaths.get(0));
//		for (int i = 0; i < imagePaths.size(); i++) {
//			System.out.println(imagePaths.get(i));
//		}
		return imagePaths;
	}
	
	//读取文件URL访问地址
	public static ArrayList<String> getImagesURL(String imgFolder, String imgURL) throws FileNotFoundException {
		ArrayList<String> imagePaths = new ArrayList<String>();
		File file = new File(imgFolder);
		File[] images = file.listFiles();
		for (int i = 0; i < images.length; i++) {
//			System.out.println(images[i].getName());
			//修复Win/Linux地址问题，不以字符串截取，使用/分割
			//C:/xtags/package/2018-06-23/81e0831ba2724514a3b3470fbcfdbbb2/000000000016.jpg
			//http://127.0.0.1:8080/xtags/2018-06-23/81e0831ba2724514a3b3470fbcfdbbb2/000000000016.jpg
			String[] apart = imgFolder.split("/");
			String relativePath0 = apart[3] + "/" + apart[4];
//			String relativePath = imgFolder.substring(17);
			imagePaths.add(imgURL + relativePath0 + "/" + images[i].getName());
		}
		System.out.println(imagePaths.get(0));
//		for (int i = 0; i < imagePaths.size(); i++) {
//			System.out.println(imagePaths.get(i));
//		}
		return imagePaths;
	}
	/**
	 * @Title: writeToJSON  
	 * @Description: 写入文件  
	 */
	public static void writeToJSON(String filepath, String content) {
		System.out.println(content);
		System.out.println(filepath);
		File file = new File(filepath);
		FileWriter fw = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file);
			fw.write(content);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @Title: readJSON  
	 * @Description: 读取文件
	 */
	public static String readJSON(String filepath) {
		System.out.println(filepath);
		StringBuilder json = new StringBuilder();
		String line = null;
		File file = new File(filepath);
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while((line = br.readLine())!=null) {
				json.append(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

}
