package com.hostel.tools;

import java.util.Map;
import java.util.Properties;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class MailSender {
	
	/**
	 * 邮箱发送验证码相关部分的配置
	 */
	private static JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
	static {
		javaMailSender.setHost("smtp.yeah.net");
		javaMailSender.setPort(465);
		javaMailSender.setUsername("ren_blue@yeah.net");
		javaMailSender.setPassword("yeahnet110");
		javaMailSender.setDefaultEncoding("UTF-8");
		
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", "true");//开启认证
//        properties.setProperty("mail.debug", "true");//启用调试
        properties.setProperty("mail.smtp.timeout", "1000");//设置链接超时
        properties.setProperty("mail.smtp.port", "465");//设置端口
        properties.setProperty("mail.smtp.socketFactory.port", "465");//设置ssl端口
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailSender.setJavaMailProperties(properties);
	}
	
	/**
	 * 发送到邮箱验证码
	 */
	public static String sendEmail(Map<String, Object> reqMap) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("Hostel World<ren_blue@yeah.net>");
		message.setTo(reqMap.get("email").toString());
		message.setSubject(reqMap.get("subject").toString());
		message.setText("From Hostel World：\n验证码：" + reqMap.get("code").toString() + "，记得在五分钟内完成验证。");
		try {
			javaMailSender.send(message);
			return "success";
		} catch (MailException e) {
			e.printStackTrace();
		}
		return "error";
	}

}
