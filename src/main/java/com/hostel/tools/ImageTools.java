package com.hostel.tools;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
  
/**
 * 通过java获取图片的宽和高 OK通过测试
 * @author sunlightcs
 * 2011-6-1
 * http://hi.juziku.com/sunlightcs/
 */
public class ImageTools {
    /**
     * 获取图片宽度
     * @param file  图片文件
     * @return 宽度
     */
    public static int[] getImgWH(String imagepath) {
    	File file = new File(imagepath);
        InputStream is = null;
        BufferedImage src = null;
        int[] WH = new int[] {-1,-1};
        try {
            is = new FileInputStream(file);
            src = javax.imageio.ImageIO.read(is);
            WH[0] = src.getWidth(); // 得到源图宽
            WH[1] = src.getHeight();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WH;
    }
    
//    public static void main(String[] args) {
//    	int[] HW = getImgWH("E:/Brian/Desktop/test2017/000000000090.jpg");
//    	System.out.println(HW[0] + "/" + HW[1]);
//    }
}