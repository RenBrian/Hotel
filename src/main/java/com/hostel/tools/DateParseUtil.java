package com.hostel.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;

public class DateParseUtil {
	
	public static String dateParseTool(Object dateStr) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = ((JSONObject)dateStr).get("time").toString();
//		System.out.println(time);
		Date newdate = new Date(Long.parseLong(time));
		String datetime = format.format(newdate);
		return datetime;
	}

}
