package com.hostel.tools;

import java.util.Random;
/**
 * 
 * @ClassName: CommonUtils  
 * @Description: 生成随机验证码 
 * @author Brian  
 * @date 2018年6月22日  
 *
 */
public class CommonUtils {

	public static String createRandomNum() {

		Random d = new Random();

		String str = "";

		for (int m = 0; m < 6; m++) {

			int num = d.nextInt(10);

			str += num + "";

		}
		return str;
	}

}
