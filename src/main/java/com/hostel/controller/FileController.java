package com.hostel.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hostel.tools.FileUtil;
import com.hostel.tools.ZipHelper;

/**
 * 
 * @ClassName: FileController  
 * @Description: 文件上传相关
 * @author Brian  
 * @date 2018年7月11日  
 *
 */
@Controller
public class FileController {
	
	@Value("${file.upload.folder}")
	private String filefolder;
	
	@Value("${file.url}")
	private String fileurl;
	
	
	@RequestMapping("/uploading")
	public String upload() {
		return "uploading";
	}
	
	
	@RequestMapping("/fileupload")
	public @ResponseBody String uploadFile(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest req, HttpServletResponse res) {
		//允许跨域访问
		res.setHeader("Access-Control-Allow-Headers", "accept, content-type");  
	    res.setHeader("Access-Control-Allow-Method", "POST");  
	    res.setHeader("Access-Control-Allow-Origin", "*");
		//重新命名UUID
		String fileName = multipartFile.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
		String filename = UUID.randomUUID().toString().replace("-", "");
		fileName = filename +"."+ suffix;
		//获取存储路径
		String filepath = "";
		String urlpath = "";
		if (suffix.equals("zip")) {
			//按照日期存储
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String subDir = sdf.format(date).toString();
			filepath = filefolder + subDir + "/";
			urlpath = fileurl + subDir + "/" + fileName;
		}
		String zipFilePath = filepath + fileName;
		System.out.println("filepath  ->>>>" + zipFilePath);
		System.out.println("urlpath ->>>>>>" + urlpath);//这个是通过URL可以访问的路径
		try {
			FileUtil.uploadFile(multipartFile.getBytes(), filepath, fileName);
			String destDir = filepath + filename + "/";
			ZipHelper.unZip(zipFilePath, destDir);
			return "{\"result\":\"success\",\"filename\":\""+ filepath + filename +"\"}";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}
	
}