package com.hostel.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hostel.entity.Page;
import com.hostel.entity.Hotel;
import com.hostel.entity.HotelVO;
import com.hostel.service.HotelService;
import com.hostel.service.RoomService;

import net.sf.json.JSONObject;
/**
 * 
 * @ClassName: HotelController  
 * @Description: 旅店相关 
 * @author Brian  
 * @date 2018年7月11日  
 *
 */
@Controller
public class HotelController {
	
	/*@RequestMapping("/release")
	public String releasePage() {
		return "/requestor/release";
	}
	//已发布的项目
	@RequestMapping("/selfproject")
	public String selfProject() {
		return "/requestor/selfproject";
	}*/
	
	@RequestMapping("/findhotel")
	public String listProject() {
		return "/worker/findproject";
	}
	
	@RequestMapping("/hotel")
	public String detailProject() {
		return "/worker/detail";
	}
	
	@Autowired
	private HotelService projectService;
	
	/**
	 * @Title: release  
	 * @Description: 添加新的酒店信息，在test里面调用，前端没有设置酒店方的界面  
	 */
	@RequestMapping("/new")
	@ResponseBody
	public String release(@RequestBody Map<String, Object> reqMap, HttpSession session) throws ParseException {
		String id = UUID.randomUUID().toString().replaceAll("-", "");
		String name = reqMap.get("name").toString();
		String addr = reqMap.get("address").toString();
		String description = reqMap.get("description").toString();
		String image = reqMap.get("image").toString();
		//session获取当前用户id
		String owner_id = session.getAttribute("userid").toString();
		String type = reqMap.get("type").toString();
		//获取yyyy-MM-dd HH:mm:ss格式的截至日期
//		String deadline = reqMap.get("dateover").toString();
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Hotel package_new = new Hotel(id, name, addr, description, image, owner_id, type, new Date());
		System.out.println(package_new.toString());
		return projectService.release(package_new);
	}
	
	//所有项目 - 分页
	@RequestMapping("/count")
	@ResponseBody
	public String countProjects() {
		
		return projectService.count() + "";
	}
	
	@RequestMapping("/listHotels")
	@ResponseBody
	public String listAllProjects(@RequestBody Map<String, Object> map) {
		Page page = new Page(
				projectService.count(),
				Integer.parseInt(map.get("current_page").toString()),
				Integer.parseInt(map.get("page_size").toString())
				);
		return projectService.allProjects(page);
	}
	
	//当前"hotel_id"存入Session
	@RequestMapping("/setHotelID")
	@ResponseBody
	public String setProjectID(HttpSession session, @RequestBody Map<String, Object> map) {
		String id = map.get("hotel_id").toString();
		try {
			session.setAttribute("hotel_id", id);
			System.out.println(id);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}
	
	//返回当前项目的信息借助session存储的VO
	@RequestMapping("/findOneVO")
	@ResponseBody
	public String findOneVO(HttpSession session) {
		HotelVO vo = (HotelVO) session.getAttribute("hotel");
		return JSONObject.fromObject(vo).toString();
	}
	
	//查找一个项目，并将VO信息存入Session - "project"
	@RequestMapping("/findOne")
	@ResponseBody
	public String findOne(HttpSession session) throws ParseException {
		String id = session.getAttribute("hotel_id").toString();
		//获取某个项目整体信息
		String project = projectService.findByID(id);
//		System.out.println(project);
		HotelVO projectVO = new HotelVO(JSONObject.fromObject(project));
		//将当前project存入session
		session.setAttribute("hotel", projectVO);
		return project;
	}
	
	//type查找
	@RequestMapping("/listByType")
	@ResponseBody
	public String listByTagType(@RequestBody Map<String, Object> map) {
		String tagtype = map.get("type").toString();
		Page page = new Page(
				projectService.countTagType(tagtype),
				Integer.parseInt(map.get("current_page").toString()),
				Integer.parseInt(map.get("page_size").toString())
				);
		return projectService.searchByTagType(tagtype, page);
	}
	//模糊搜索
	@RequestMapping("/listByKeyWord")
	@ResponseBody
	public String listByKeyWord(@RequestBody Map<String, Object> map) {
		String keyword = map.get("keyword").toString();
		Page page = new Page(
				projectService.countKey(keyword),
				Integer.parseInt(map.get("current_page").toString()),
				Integer.parseInt(map.get("page_size").toString())
				);
		return projectService.searchByWords(keyword, page);
	}
	
	@Autowired
	private RoomService roomService;
	
	@RequestMapping("/getRooms")
	@ResponseBody
	public String getRooms(HttpSession session) {
		String hotel_id = session.getAttribute("hotel_id").toString();
		System.out.println(roomService.getRooms(hotel_id));
		return roomService.getRooms(hotel_id);
	}
	
}
