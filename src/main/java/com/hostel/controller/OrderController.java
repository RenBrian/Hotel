package com.hostel.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hostel.entity.Order;
import com.hostel.service.OrderService;

@Controller
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * @Title: newTask  
	 * @Description: 点击预定形成新的订单
	 */
	@RequestMapping("/newOrder")
	@ResponseBody
	public String newOrder(@RequestBody Map<String, Object> reqMap, HttpSession session) throws ParseException {
		String user_id = session.getAttribute("userid").toString();
		String hotel_id = session.getAttribute("hotel_id").toString();
		Order order = new Order(
				UUID.randomUUID().toString().replaceAll("-", ""),
				reqMap.get("order_name").toString(),
				Double.parseDouble(reqMap.get("price").toString()),
				user_id,
				hotel_id,
				reqMap.get("room_id").toString(),
				reqMap.get("order_on_off").toString(),
				new Date());
		System.out.println("order:" + orderService.newOrder(order));
		return orderService.newOrder(order);
	}
	
	@RequestMapping("/allOrders")
	@ResponseBody
	public String allOrders(HttpSession session) throws ParseException {
		String user_id = session.getAttribute("userid").toString();
		return orderService.allOrder(user_id);
	}
	
	@RequestMapping("/cancelOrders")
	@ResponseBody
	public String cancelOrders(@RequestBody Map<String, Object> reqMap) {
		String order_id = reqMap.get("order_id").toString();
		return orderService.cancel(order_id);
	}
	
	@RequestMapping("/payOrders")
	@ResponseBody
	public String payOrders(@RequestBody Map<String, Object> reqMap) {
		String order_id = reqMap.get("order_id").toString();
		return orderService.paid(order_id);
	}
	
}
