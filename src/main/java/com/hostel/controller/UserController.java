package com.hostel.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hostel.entity.User;
import com.hostel.service.UserService;

import net.sf.json.JSONObject;

/**
 * @ClassName: UserController  
 * @Description: 个人信息模块 - 登陆 、注册、 查看 、 修改个人信息
 * @author Brian  
 * @date 2018年3月24日  
 */
@Controller
public class UserController {
	/**
	 * 页面相关部分
	 */
	/**
	 * @Title: login  
	 * @Description: 返回登陆页面 
	 */
	@RequestMapping("/login")
	public String login() {
		return "/login";
	}
	//注销
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "/login";
	}
	//页面 - 注册
	@RequestMapping("/register")
	public String register() {
		return "/register";
	}
	//主页
	@RequestMapping("/index")
	public String index(HttpSession session) {
		return "/worker/index";
	}
	
	/**
	 * @Title: userinfo  
	 * @Description: 个人中心页面，当前session无用户则返回登陆界面
	 */
	@RequestMapping("/usercenter")
	public String userinfo(HttpSession session) {
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
		} catch (Exception e) {
			System.out.println("session no user @ UserController 94");
		}
		if (userid==null) {
			return "/login";
		}
		return "/worker/workerinfo";
	}
	
	/**
	 * json数据请求响应部分
	 * @Title : getUserName
	 * @Description: 获取当前session存储的用户信息
	 */
	@RequestMapping("/getSessionUser")
	@ResponseBody
	public String getSessionUser(HttpSession session) {
		Map<String, Object> map = new HashMap<String, Object>();
		String username = null;
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
			username = session.getAttribute("username").toString();
		} catch (Exception e) {
			System.out.println("session no user");
		}
		if (username!=null) {
			map.put("userid", userid);
			map.put("username", username);
			map.put("result", "success");
			return JSONObject.fromObject(map).toString();
		}
		map.put("result", "error");
		return JSONObject.fromObject(map).toString();
	}
	
	
	@Autowired
	private UserService userService;
	/**
	 * @Title: login  
	 * @Description: 登陆验证，通过则存储当前用户的基本信息在session中
	 */
	@RequestMapping("/loginValidation")
	public @ResponseBody String login(@RequestBody Map<String, Object> reqMap, HttpSession session) {
		String email = reqMap.get("email").toString();
		String password = reqMap.get("password").toString();
		User user = userService.userLogin(email, password);
		System.out.println(user);
		if (user!=null) {
			//存储当前登陆user以及user_id
			session.setAttribute("user", user);
			session.setAttribute("username", user.getName());
			session.setAttribute("userid", user.getId());
			return "{\"result\":\"success\"}";
		}else {
			return "{\"result\":\"error\"}";
		}
	}
	
	/**
	 * @Title: getuserinfo  
	 * @Description: 返回当前用户完整信息 
	 */
	@RequestMapping("/userinfo")
	public @ResponseBody String getuserinfo(HttpSession session) {
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
		} catch (Exception e) {
			System.out.println("/userinfo - session no user");
		}
		if (userid==null) {
			return null;
		}
		return userService.userInfo(userid);
	}
	
	/**
	 * @Title: register  
	 * @Description: 注册新用户，返回操作结果 
	 */
	@RequestMapping("/signup")
	public @ResponseBody String register(@RequestBody Map<String, Object> reqMap) {
		String userid = UUID.randomUUID().toString().replaceAll("-", "");
		//获取当前时间
		Date currentTime = new Date();  
		String username = reqMap.get("username").toString();
		String password = reqMap.get("password").toString();
		String email = reqMap.get("email").toString();
		User newuser = new User(userid, username, email, password, currentTime);
		
		return userService.userRegister(newuser);
	}
	
	@RequestMapping("/updatePassword")
	@ResponseBody
	public String updatePassword(@RequestBody Map<String, Object> map, HttpSession session) {
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
		} catch (Exception e) {
			System.err.println("/updateInfo session no user");
			return "{\"result\":\"error\"}";
		}
		String oldpassword = map.get("oldpassword").toString();
		String newpassword = map.get("newpassword").toString();
		return userService.updatePassword(userid, oldpassword, newpassword);
	}
	
	@RequestMapping("/updateBalance")
	@ResponseBody
	public String updateBalance(@RequestBody Map<String, Object> map, HttpSession session) {
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
		} catch (Exception e) {
			System.err.println("/updateBalance session no user");
			return "{\"result\":\"error\"}";
		}
		double balance = Double.parseDouble(map.get("balance").toString());
		return userService.updateBalance(userid, balance);
	}
	
	/**
	 * @Title: updateInfo  
	 * @Description: 更改用户名、身份证号、手机号等  
	 */
	@RequestMapping("/updateInfo")
	@ResponseBody
	public String updateInfo(@RequestBody Map<String, Object> map, HttpSession session) {
		String userid = null;
		try {
			userid = session.getAttribute("userid").toString();
		} catch (Exception e) {
			System.err.println("/updateInfo session no user");
			return "{\"result\":\"error\"}";
		}
		String name = map.get("new_name").toString();
		String identity = map.get("new_identity").toString();
		String tele = map.get("new_tele").toString();
		return userService.updateInfo(identity, tele, name, userid);
	}
	
}
