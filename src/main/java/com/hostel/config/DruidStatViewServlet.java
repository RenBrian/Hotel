package com.hostel.config;

import com.alibaba.druid.support.http.StatViewServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(
			urlPatterns= {"/druid/*"} , 
			initParams = {
					//
//					@WebInitParam(name="allow" , value="127.0.0.1") ,
//					@WebInitParam(name="deny" , value="") ,
					@WebInitParam(name="loginUsername" , value="xtags") , 
					@WebInitParam(name="loginPassword" , value="xtags") , 
					@WebInitParam(name="resetEnable" , value="true")
			}
		)
public class DruidStatViewServlet extends StatViewServlet {
	private static final long serialVersionUID = 1L;
}
