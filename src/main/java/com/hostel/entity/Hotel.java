package com.hostel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
 
@Entity
@Table(name="x_hotel")
@Data
public class Hotel implements Serializable{
	private static final long serialVersionUID = -5966069863493177864L;
	@Id
	private String id;
	private String name;//旅店名称
	private String address;
	private String description;//介绍
	private String image;//图片url
	private String tele;//联系电话
	private String type;//种类  Hotel/Homestay/Inn  酒店、民宿、客栈（青旅）
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;//建立日期
	private boolean isFull;//满客
	private double star;//评级5.0为基数
	
	
	public Hotel() {
		super();
	}
	
	//新建
	public Hotel(String id, String name, String addr, String description, String tele, String image, String type, Date date) {
		super();
		this.id = id;
		this.name = name;
		this.address = addr;
		this.description = description;
		this.tele = tele;
		this.image = image;
		this.type = type;
		this.date = date;
		
		this.isFull = false;
		this.star = -1;//暂无评分
	}
}
