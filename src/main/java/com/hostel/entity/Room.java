package com.hostel.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="x_room")
@Data
public class Room {
	
	@Id
	private String id;
	private String name;//001 002 666
	private String hotel_id;
	private String type; //Single/Double/Triple/Size/Suite/Bed 单人间、双人间(标间)、三人间、大床房、套房、床位（青旅）
	
	private double price;
	private String description;//条件说明 网络、海景、......
	
	private boolean status;//入住状态   in 1 / empty 0
	
	public Room() {
		super();
	}

	public Room(String id, String name, String hotel_id, String type, double price, String description) {
		super();
		this.id = id;
		this.name = name;
		this.hotel_id = hotel_id;
		this.type = type;
		this.price = price;
		this.description = description;
		
		this.status = false;
	}
	
}
