package com.hostel.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.hostel.tools.DateParseUtil;

import lombok.Data;
import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: 
 * @Description: 与众包相关的暂存项目 
 * @author Brian  
 * @date 2018年6月22日  
 *
 */
@Data
public class HotelVO {
	
	private String id;
	private String name;//旅店名称
	private String address;
	private String description;//介绍
	private String image;//图片url
	private String tele;//联系电话
	private String type;//种类  Hotel/Homestay/Inn  酒店、民宿、客栈（青旅）
	private String date;//建立日期
	private boolean isFull;//满客
	private double star;//评级5.0为基数
	
	private ArrayList<Room> rooms;
	
	public HotelVO(JSONObject json) {
		super();
		this.id = json.getString("id");
		this.name = json.getString("name");
		this.address = json.getString("address");
		this.description = json.getString("description");
		this.tele = json.getString("tele");
		this.type = json.getString("type");
		this.isFull = json.getBoolean("full");
		this.star = json.getDouble("star");
		
		this.date = DateParseUtil.dateParseTool(json.get("date"));
		
		rooms = null;
	}
	
	public HotelVO(Hotel hotel) {
		super();
		this.id = hotel.getId();
		this.name = hotel.getName();
		this.address = hotel.getAddress();
		this.description = hotel.getDescription();
		this.image = hotel.getImage();
		this.tele = hotel.getTele();
		this.type = hotel.getType();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.date = sdf.format(hotel.getDate());
		
		this.isFull = hotel.isFull();
		this.star = hotel.getStar();
		
		//获取房间信息
		rooms = null;
	}

}
