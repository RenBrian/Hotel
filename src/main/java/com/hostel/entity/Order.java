package com.hostel.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
/**
 *
 */
@Entity
@Table(name="x_order")
@Data
public class Order implements Serializable{

	private static final long serialVersionUID = -375682375208623448L;
	@Id
	private String id;
	private String order_name;//hotel_name + 时间
	private double price;
	
	private String user_id;
	private String hotel_id;
	private String room_id;
	private String order_on_off;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date order_time;
	
	private String status;//订单状态   topay / canceled / complete  待付款、取消、已完成  规定预定 30 min内需要付款
	
	
	public Order() {
		super();
	}


	public Order(String id, String order_name, double price, String user_id, String hotel_id, String room_id, String time_in_out ,Date order_time) {
		super();
		this.id = id;
		this.order_name = order_name;
		this.price = price;
		this.user_id = user_id;
		this.hotel_id = hotel_id;
		this.room_id = room_id;
		this.order_on_off = time_in_out;
		this.order_time = order_time;
		
		this.status = "topay";//默认生成订单为待付款状态
	}
	
	
}
