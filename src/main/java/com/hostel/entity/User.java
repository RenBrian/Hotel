package com.hostel.entity;

import java.io.Serializable;
//import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

/**
 * @desc 用户实体类
 * @author st0001
 *
 */
@Entity
@Table(name="x_user")
@Data
public class User implements Serializable{
	
	private static final long serialVersionUID = 1696360673407775410L;
	
	@Id
	@Column(columnDefinition="char(32) not null")
	private String id;//主键32位唯一标识
	@Column(columnDefinition="varchar(32) not null")
	private String name;
	@Column(columnDefinition="varchar(32) not null")
	private String email;
	
	
	@Column(columnDefinition="varchar(18)")
	private String identity;//身份证号
	@Column(columnDefinition="varchar(11)")
	private String tele;//手机号码
	private String password;
	
	private double balance;//余额
	private int point;
	private int level;
	
	private String Status;//状态
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date date;
	
	public User() {
		super();
	}
	//新建用户
	public User(String id, String name, String email, String password, Date date) {
		super();
		//必须
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.date = date;
		
		this.balance = 0;
		this.point = 0;
		this.level = 0;
		this.Status = "Less";//Normal or Less 余额充足与余额不足
	}

}
