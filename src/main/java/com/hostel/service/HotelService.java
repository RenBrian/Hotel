package com.hostel.service;

import com.hostel.entity.Page;
import com.hostel.entity.Hotel;

public interface HotelService {
	//发布项目，返回是否成功
	public String release(Hotel package_new);
	
	
	//ID查找单个项目
	public String findByID(String project_id);
	
	//count
	public int count();
	//分页查找所有项目，时间倒序
	public String allProjects(Page page);
	
	//按照类型查找项目
	public int countTagType(String tag_type);
	public String searchByTagType(String tag_type, Page page);
	//搜索框查找
	public int countKey(String keyword);
	public String searchByWords(String keyword, Page page);

}
