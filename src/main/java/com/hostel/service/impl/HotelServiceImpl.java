package com.hostel.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hostel.dao.HotelDao;
import com.hostel.service.HotelService;
import com.hostel.entity.Page;
import com.hostel.entity.Hotel;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


@Service
public class HotelServiceImpl implements HotelService{
	
	@Autowired
	private HotelDao packageDao;
	
	@Override
	public int count() {
		return (int)packageDao.count();
	}

	//新增一个hotel
	@Override
	@Transactional
	public String release(Hotel package_new) {
		Hotel pack = packageDao.save(package_new);
		if (pack!=null) {
			return "{\"result\":\"success\",\"id\":\""+ pack.getId() +"\"}";
		}
		return "{\"result\":\"error\"}";
	}

	//按照ID查询
	@Override
	public String findByID(String project_id) {
		Hotel project = packageDao.findOne(project_id);
		JSONObject json = JSONObject.fromObject(project);
		return json.toString();
	}
	
	//按照标注类型查找
	@Override
	public int countTagType(String tagtype) {
		return (int) packageDao.countTagType(tagtype);
	}
	@Override
	public String searchByTagType(String tag_type, Page page) {
		List<Hotel> projects = packageDao.findByTagType(tag_type, page.getOffset_row(), page.getPage_size());
		JSONArray json = JSONArray.fromObject(projects);
		return json.toString();
	}
	
	//按照项目名称模糊查询
	@Override
	public int countKey(String keyword) {
		String key_word = "%" + keyword + "%";
		return (int) packageDao.countKey(key_word);
	}
	@Override
	public String searchByWords(String keyword, Page page) {
		String key_word = "%" + keyword + "%";
		List<Hotel> projects = packageDao.findByNameLike(key_word, page.getOffset_row(), page.getPage_size());
		JSONArray json = JSONArray.fromObject(projects);
		return json.toString();
	}

	//查找所有项目
	@Override
	public String allProjects(Page page) {
		List<Hotel> projects = packageDao.findAllProjects(page.getOffset_row(), page.getPage_size());
		return JSONArray.fromObject(projects).toString();
	}
	
//	@Override
//	@Transactional
//	public String mark(String project_id) {
//		try {
//			packageDao.mark(project_id);
//			return "{\"result\":\"success\"}";
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return "{\"result\":\"error\"}";
//	}

}
