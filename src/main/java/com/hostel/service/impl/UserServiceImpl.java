package com.hostel.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hostel.dao.UserDao;
import com.hostel.entity.User;
import com.hostel.service.UserService;

import net.sf.json.JSONObject;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	
	
	/**
	 * 登陆验证，先检查是否能够找到响应邮箱的账户
	 */
	@Override
	public User userLogin(String email, String password) {
		User user = userDao.findByEmail(email);
		if (user!=null && user.getPassword().equals(password)) {
			return user;
		}
		return null;
	}

	/**
	 * 新增用户，限制是一个邮箱仅可注册一个账户
	 */
	@Override
	@Transactional
	public String userRegister(User user) {
		if (!usedEmail(user.getEmail())) {
			return "{\"result\":\"used\"}";
		}
		User user_insert = userDao.save(user);
		if (user_insert!=null) {
			return "{\"result\":\"success\",\"userid\":\""+ user_insert.getId() +"\"}";
		}
		return "{\"result\":\"error\"}";
	}
	
	/**
	 * 根据ID寻找用户，返回json数据
	 */
	@Override
	public String userInfo(String userid) {
		User worker = userDao.findOne(userid);
		JSONObject js = JSONObject.fromObject(worker);
		return js.toString();
	}
	
	/**
	 * 更新密码
	 */
	@Override
	@Transactional
	public String updatePassword(String userid, String oldpassword, String newpassword) {
		if (userDao.findOne(userid).getPassword().equals(oldpassword)) {
			try {
				userDao.updatePassword(newpassword, userid);
				return "{\"result\":\"success\"}";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "{\"result\":\"error\"}";
		
	}
	/**
	 * 更新账户余额
	 */
	@Override
	@Transactional
	public String updateBalance(String userid, double balance) {
		try {
			userDao.updateBalance(balance, userid);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}
	
	/**
	 * @Title: updateInfo  
	 * @Description: 更新其他信息 
	 */
	@Override
	@Transactional
	public String updateInfo(String identity, String tele, String name, String id) {
		try {
			userDao.updateInfo(identity, tele, name, id);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}
	
	
	/**
	 * 中间工具类，查看某个邮箱是否已被占用
	 */
	@Override
	public boolean usedEmail(String email) {
		User user = userDao.findByEmail(email);
		System.out.println(user);
		if (user!=null) {
			return false;
		}
		return true;
	}
	
}
