package com.hostel.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hostel.dao.RoomDao;
import com.hostel.entity.Room;
import com.hostel.service.RoomService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class RoomServiceImpl implements RoomService{
	
	@Autowired
	private RoomDao roomDao;

	@Override
	@Transactional
	public String addRoom(Room room) {
		try {
			roomDao.save(room);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}

	@Override
	@Transactional
	public String checkin(String id) {
		try {
			roomDao.checkin(id);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}

	@Override
	@Transactional
	public String checkout(String id) {
		try {
			roomDao.checkout(id);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"result\":\"error\"}";
	}
	
	//找到同一旅店下的房间
	@Override
	@Transactional
	public String getRooms(String hotel_id) {
		List<Room> rooms = roomDao.findByHotelID(hotel_id);
		JSONArray json = JSONArray.fromObject(rooms);
		return json.toString();
	}

}
