package com.hostel.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hostel.dao.OrderDao;
import com.hostel.dao.RoomDao;
import com.hostel.entity.Order;
import com.hostel.service.OrderService;

import net.sf.json.JSONArray;
@Service
public class OrderServiceImpl implements OrderService{
	
	
	@Autowired
	private OrderDao taskDao;
	
	@Autowired
	private RoomDao roomDao;

	@Override
	@Transactional
	public String newOrder(Order task) {
		Order task_new = taskDao.save(task);
		if (task_new != null) {
			return "{\"result\":\"success\",\"userid\":\""+ task_new.getId() +"\"}";
		}
		return "{\"result\":\"error\"}";
	}
	
	@Override
	public String allOrder(String userid) {
		List<Order> orders = taskDao.all(userid);
		return JSONArray.fromObject(orders).toString();
	}
	
	@Override
	@Transactional
	public String cancel(String orderid) {
		try {
			taskDao.cancel(orderid);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();		
		}
		return "{\"result\":\"error\"}";
	}
	
	@Override
	@Transactional
	public String paid(String orderid) {
		try {
			taskDao.complete(orderid);
			return "{\"result\":\"success\"}";
		} catch (Exception e) {
			e.printStackTrace();		
		}
		return "{\"result\":\"error\"}";
	}

}
