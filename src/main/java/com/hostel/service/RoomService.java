package com.hostel.service;

import org.springframework.stereotype.Service;

import com.hostel.entity.Room;

@Service
public interface RoomService {
	
	public String addRoom(Room room);
	
	public String checkin(String id);
	
	public String checkout(String id);
	
	public String getRooms(String hotel_id);

}
