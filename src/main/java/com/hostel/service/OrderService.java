package com.hostel.service;

import com.hostel.entity.Order;

public interface OrderService {
	
	
	public String newOrder(Order task);
	
	public String allOrder(String userid);
	
	public String cancel(String orderid);
	
	public String paid(String orderid);
	
}
