package com.hostel.service;

import com.hostel.entity.User;

public interface UserService {
	
	/**
	 * @Title: userLogin  
	 * @Description: 用户登陆  
	 */
	public User userLogin(String email, String password);
	
	/**
	 * @Title: userRegister  
	 * @Description: 用户注册  
	 */
	public String userRegister(User worker);
	
	/**
	 * @Title: userInfo  
	 * @Description: 获取用户信息  
	 */
	public String userInfo(String userid);

	public boolean usedEmail(String email);
	
	//更改密码
	public String updatePassword(String userid, String oldpassword, String newpassword);
	
	//更新账户余额
	public String updateBalance(String userid, double balance);
	
	//更新其他信息
	public String updateInfo(String identity, String tele, String name, String id);
	
}
