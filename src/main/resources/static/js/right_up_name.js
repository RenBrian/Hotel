$().ready(function() {
	
	$.ajax({
        url: "/hostel/getSessionUser",
        type: 'post',
        contentType:'application/json',
        dataType:"json",
		timeout:5000,
        success: function (data) {
//        	console.log(data);
        	var status = data.result;
        	if(status == "success") {
				console.log(data.username);			
				$("#login_or").html("登出");
				$("#username").html(data.username);
			}else{
				$("#login_or").html("登入");
				toastr.info("未登陆");
			}
        },error:function() {
        	console.log("未获取个人信息");
        }
	});
	
})