/**
 * Created by st0001 on 2017/12/14.
 */
$().ready(function() {
    $("#login_form").validate({
        rules: {
        	username:{
        		required: true,
        		minlength: 2
        	},
        	userpassword: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            username: {
            	required: "请输入用户名",
            	minlength: "请检查用户名"
            },
            userpassword: {
                required: "请输入密码",
                minlength: "密码不能小于{0}个字符"
            }
        }
    });
    
    $("#login").click(function(){
    	var valid = $("#login_form").valid();
    	if(!valid){
    		toastr.warning("检查输入！");
    	}else{
        	var name=$("#username").val();
        	var passwd=$("#userpassword").val();
        	
        	$.ajax({
                url: "/hostel/loginValidation",
                type: 'post',
                contentType:'application/json',
                data:JSON.stringify({
                	"email":name,
        			"password":passwd
                }), 
                dataType:"json",
    			timeout:5000,
                success: function (data) {
                	console.log(data);
                    var status = data.result;
                    if (status == "error") {
                    	toastr.warning("用户名或密码不正确！");
    				}else if (status == "success") {
    					window.location = "/hostel/index";
    				}
                },
                error: function(e){    // 失败后回调
                	console.log(e);
                    toastr.error("服务器出错");
                }
        	});
    	}
    
    });
    
});