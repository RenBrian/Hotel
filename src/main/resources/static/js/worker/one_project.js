Date.prototype.format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

$().ready(function() {
	var user_type = null;
	var worker_id = null;
	
	/**
	 * 得到某个酒店的信息
	 */
	$.ajax({
		async: false,
	    type: "GET",
	    cache:true, 
	    dataType: 'json',
	    url: "/hostel/findOne",
	    timeout: 3000,
	    contentType: "application/json;utf-8",
	    success: function(msg) {
	    	console.log(msg);
	    	
	    	var imageurl = msg.image.split(";");
	    	var imageHTML = '<div class="carousel-item active">'
	    		+ '<img class="d-block w-100" src="'+ imageurl[0] +'" alt="First slide"></div>'
	    		+ '<div class="carousel-item">'
	    		+ '<img class="d-block w-100" src="'+ imageurl[1] +'" alt="Second slide"></div>'
	    		+ '<div class="carousel-item">'
	    		+ '<img class="d-block w-100" src="'+ imageurl[2] +'" alt="Third slide"></div>';
		    
	    	$("#hotel_image").html(imageHTML);
	    	
	    	$("#hotel_name").html(msg.name);
	    	$("#hotel_description").html(msg.description);
	    	$("#hotel_star").html(msg.star);
        	$("#hotel_contact").html(msg.tele);
	    }
	});
	
	
	//hotel_table
	$.ajax({
		async: false,
	    type: "GET",
	    cache:true, 
	    dataType: 'json',
	    url: "/hostel/getRooms",
	    timeout: 3000,
	    contentType: "application/json;utf-8",
	    success: function(msg) {
	    	
	    	description	    	:	    	"单人间；网络；早餐"
	    	hotel_id	    	:	    	"1d77e51acd4a4c7583d5fe46779cb28d"
	    	id	    	:	    	"0f776ffb47b0413db01744eb1cdde640"
	    	name	    	:	    	"101"
	    	price	    	:	    	299
	    	status	    	:	    	false
	    	type	    	:	    	"Single"
	    	
	    	console.log(msg);
			var roomHTML = '<thead><tr><th hidden="hidden" id="project_id"></th>'
				+ '<th style="width:20%">房间号</th>'
				+ '<th style="width:25%">条件</th>'
				+ '<th style="width:15%">价格（￥/天）</th>'
				+ '<th style="width:15%">预定</th></tr></thead><tbody>';
	    	for (var i = 0; i < msg.length; i++) {
				var one = '<tr><td hidden="hidden">' + msg[i].id + '</td>'
					+ '<td><a>' + msg[i].name + '</a></td>'
					+ '<td>' + msg[i].description + '</td>'
					+ '<td style="color: green;">'+ msg[i].price + '</td>';
				if (msg[i].status==false) {
					one += '<td><a href="#" name="hotel_reserve">预定</a></td></tr>';
				}else{
					one += '<td><a href="" style="color: grey;">预定（已入住）</a></td>';
				}
				roomHTML += one;
			}
	    	
		    roomHTML += '</tbody>';
	    	
        	$("#hotel_table").html(roomHTML);
	    }
	});
	
	$(document).on('click',"a[name='hotel_reserve']",function(e){
		//获取信息，生成预订单
		var room_id = $(this).parent().parent().find('td:first').text();
		var price = $(this).parent().parent().find('td:first').next().next().next().text();
		console.log(price);
		var check_in = $("#datecheckin").val();
		var check_out = $("#datecheckout").val();
		var order_on_off = check_in + "~" + check_out;
		var order_name = $("#hotel_name").text() + (new Date()).format("yyyy-MM-dd-hh-mm-ss");
		
		//生成Order
		$.ajax({
			async: false,
	        url: "/hostel/newOrder",
	        type: 'post',
	        contentType:'application/json',
	        dataType:"json",
	        data:JSON.stringify({
		    	"order_name":order_name,
		    	"price":price,
		    	"room_id":room_id,
		    	"order_on_off":order_on_off
		    }),
			timeout:5000,
	        success: function (data) {
	        	console.log(data);
	        	var status = data.result;
	        	if(status == "success") {
	        		window.location="/hostel/usercenter";
				}else{
					toastr.warning("预定失败");
				}
	        },error:function() {
	        	toastr.warning("未获取");
	        }
		});
		
	});
	
	
});