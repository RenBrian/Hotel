Date.prototype.format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

$().ready(function() {
	var user_balance = null;
	
	
	$.ajax({
		async:false,
        url: '/hostel/userinfo',
        type: 'post',
        contentType:'application/json',
        dataType:"json",
		timeout:20000,
        success: function (data) {
//        	console.log(data);
        	user_balance = data.balance;
        	$("#userid").html(data.id);
        	$("#name").html(data.name);
        	$("#point").html(data.point);
        	$("#level").html(data.level);
        	$("#email").html(data.email);
        	$("#balance").html("￥" + data.balance);
        	
        	var id_number = "";
        	if (data.identity=="" || data.identity==null) {
			}else{
				id_number = data.identity.substring(0,6) + "********" + data.identity.substring(14,18);
			}
        	$("#id_number").val(id_number);
        	$("#tele").val(data.tele);
        },error:function(msg) {
        	console.log("fail");
        }
	});
	
	//请求历史信息订单
	$.ajax({
		async:false,
        url: '/hostel/allOrders',
        type: 'post',
        contentType:'application/json',
        dataType:"json",
		timeout:20000,
        success: function (data) {
        	console.log(data);
	
			var orderHTML = "";
			for (var i = 0; i < data.length; i++) {
				console.log(data[i].price);
				var one = '<tr><td hidden="hidden">'+ data[i].id +'</td>'
					+ '<td>'+ data[i].order_name +'</td>'
					+ '<td>'+ data[i].price +'</td>';
				if (data[i].status=='topay') {
					one += '<td><a style="color: green;" href="#" name="topay">去付款</a> | <a style="color: red;" href="#" name="tocancel">取消</a></td>';
				}else if(data[i].status=='canceled'){
					one += '<td style="color: gray;">已取消</td>';
				}else {
					one += '<td style="color: blue;">已完成</td>';
				}
				
				one += '<td>'+ data[i].order_on_off +'</td></tr>';
				orderHTML += one;
			}
        	$("#order_list").html(orderHTML);
	
        },error:function(msg) {
        	console.log("fail");
        }
	});
	
	
	//更改信息，身份证号与手机号等
	$(document).on('click','#confirm',function(e){
		var name = $("#name").text();
		var id_number = $("#id_number").val();
		var tele = $("#tele").val();
		$.ajax({
			async:false,
	        url: '/hostel/updateInfo',
	        type: 'post',
	        contentType:'application/json',
	        dataType:"json",
	        data:JSON.stringify({
	        	"new_name":name,
    			"new_identity":id_number,
    			"new_tele":tele
            }),
			timeout:20000,
	        success: function (data) {
	        	console.log(data);
	        	window.location = "/hostel/usercenter";
	        },error:function(msg) {
	        	console.log("fail");
	        }
		});
	});
	
	
	$(document).on('click',"a[name='topay']",function(e){
		var order_id = $(this).parent().parent().find('td:first').text();
		var order_price = $(this).parent().parent().find('td:first').next().next().text();
		if (user_balance < order_price) {
			toastr.warning("账户余额不足!");
		}else{
			if (confirm("确认付款？")) {
				//调整余额 + 状态调整
				var new_balance = user_balance - order_price;
				$.ajax({
					async:false,
			        url: '/hostel/updateBalance',
			        type: 'post',
			        contentType:'application/json',
			        dataType:"json",
			        data:JSON.stringify({
			        	"balance":new_balance    			
		            }),
					timeout:20000,
			        success: function (data) {
			        	console.log(data);
			        	$.ajax({
			    			async:false,
			    	        url: '/hostel/payOrders',
			    	        type: 'post',
			    	        contentType:'application/json',
			    	        dataType:"json",
			    	        data:JSON.stringify({
			    	        	"order_id":order_id    			
			                }),
			    			timeout:20000,
			    	        success: function (data) {
			    	        	console.log(data);
			    	        },error:function(msg) {
			    	        	console.log("fail");
			    	        }
			    		});
			        	window.location = "/hostel/usercenter";
			        },error:function(msg) {
			        	console.log("fail");
			        }
				});
			}
		}
	});
	
	$(document).on('click',"a[name='tocancel']",function(e){
		var order_id = $(this).parent().parent().find('td:first').text();
		$.ajax({
			async:false,
	        url: '/hostel/cancelOrders',
	        type: 'post',
	        contentType:'application/json',
	        dataType:"json",
	        data:JSON.stringify({
	        	"order_id":order_id    			
            }),
			timeout:20000,
	        success: function (data) {
	        	console.log(data);
	        	window.location = "/hostel/usercenter";
	        },error:function(msg) {
	        	console.log("fail");
	        }
		});
	});
	
});