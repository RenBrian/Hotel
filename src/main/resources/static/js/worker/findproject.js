Date.prototype.format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

$().ready(function() {
	
	var page_size = 8;
	var current_page = 1;
	var total_page;
	
	var listTerm;
	
	var countAll;
	
	$(document).on('click','[name="button"]',function(e){
		
	});
	
	$.ajax({
		async: false,
	    type: "GET",
	    cache:true, 
	    dataType: 'json',
	    url: "/hostel/count",
	    timeout: 3000,
	    contentType: "application/json;utf-8",
	    success: function(msg) {
	    	countAll = msg;
	    	total_page = Math.ceil(msg/page_size);
	    	var html = '<li id="pre"><a href="#">&laquo;</a></li>';
	    	for(var i=0;i<total_page;i++)
	    		html += '<li id="page_'+(i+1)+'"><a href="#">'+(i+1)+'</a></li>';
	    	html += '<li id="next"><a href="#">&raquo;</a></li>';
	    	$(".pagination").html(html);
	    	$(".pagination").find("#page_" + current_page).addClass("active");
	    }
	});
	
	function listbyPage() {
		$.ajax({
			async: false,
		    type: "POST",
		    cache:true, 
		    dataType: 'json',
		    url: "/hostel/listHotels",
		    data:JSON.stringify({
		    	"current_page":current_page,
		    	"page_size":page_size,
		    }),
		    timeout: 3000,
		    contentType: "application/json;utf-8",
		    success: function(msg) {
		    	console.log(msg);
		    	var hotelHTML = "";
		    	for (var i = 0; i < msg.length; i++) {
		    		var desc = msg[i].description;
		    		if (desc.length > 40) {
						desc = desc.substring(0,40) + "......";
					}
		    		//只显示第一张图片
		    		var image = msg[i].image.split(";")[0];
		    		var one = '<div class="card pull-left" style="width: 270px;height: 400px">'
		    			+ '<img class="card-img-top" width="240px" height="180px" src="' + image + '" alt="Card image cap">'
		    			+ '<div class="card-body">'
		    			+ '<h5 class="card-title">' + msg[i].name + '</h5>'
		    			+ '<p class="card-text">' + desc + '</p>'
		    			+ '<a href="#" name="hotel_detail" class="btn btn-primary-flat">查看</a>'
		    			+ '<label hidden="hidden">' + msg[i].id + '</label>'
		    			+ '</div></div>';
		    			// (new Date(msg[i].date.time)).format("yyyy-MM-dd")
		    		hotelHTML += one;
		    	}
		    	
		    	$('#hotel_list').html(hotelHTML);
//		    	console.log("success");
		    },error: function(e){
		    	console.log(e);
		    }
		});
	}
	
	listbyPage();
	//依据页面情况上下页禁用
	if(current_page==1){
		$("#pre").addClass("disabled");
		$("#pre>a").attr('disabled',true);
	}
	else{
		$("#pre").removeClass("disabled");
		$("#pre>a").attr('disabled',false);
	}
	
	if(current_page==total_page){
		$("#next").addClass("disabled");
		$("#next>a").attr('disabled',true);
	}
	else{
		$("#next").removeClass("disabled");
		$("#next>a").attr('disabled',false);
	}
	//上一页下一页
	$(document).on('click',".pagination>li",function(e){ 
		//console.log("id:"+$(this).attr("id"));
		if($(this).attr("id")=="pre"&&current_page!=1){
			$("#page_"+current_page).removeClass("active");
			current_page--;
			$("#page_"+current_page).addClass("active");
			//console.log('a');
		}else if($(this).attr("id")=="next"&&current_page!=total_page){
			$("#page_"+current_page).removeClass("active");
			current_page++;
			$("#page_"+current_page).addClass("active");
			//console.log('b');
		}else if($(this).attr("id")!="pre"&&$(this).attr("id")!="next"){
			$("#page_"+current_page).removeClass("active");
			$(this).addClass("active");
			current_page = $(this).text();
			//console.log('c');
		}
		
		listbyPage();
		//console.log(currentPage);
		if(current_page==1){
			$("#pre").addClass("disabled");
		}
		else{
			$("#pre").removeClass("disabled");
		}
		
		if(current_page==total_page){
			$("#next").addClass("disabled");
		}
		else{
			$("#next").removeClass("disabled");
		}
	});
	
	$(document).on('click',"a[name='hotel_detail']",function(e){
		var id = $(this).next().text();
		console.log(id);
		$.ajax({
			async: false,
		    type: "POST",
		    cache:false, 
		    url: "/hostel/setHotelID",
		    timeout: 3000,
		    data:JSON.stringify({
		    	"hotel_id":id
		    }),
		    contentType: "application/json;utf-8",
		    success: function(msg) {
		    	window.location="/hostel/hotel";
		    }
		});
	});
	
});