$().ready(function() {
	var currentURL = "";
	var currentindex = 0;
	var currentTagType = null;//记录当前标注类型
	
	/*award:10
	date:"2018-06-23 05:26:04"
	dateover:"2018-09-01 00:00:00"
	description:"标注图中出现的物体，如某种水果，或者生活用品。"
	id:"41045b38cf1c4d9aaf09da1cbd7c603d"
	imagesPath:(13) 
		["C:/xtags/package/2018-06-23/530e8bf833c545e3a4a5e86cc97f5f79/000000533977.jpg", 
		"C:/xtags/package/2018-06-23/530e8bf833c545e3a4a5e86cc97f5f79/000000534199.jpg"...]
	imagesURL:(13) 
		["http://127.0.0.1:8080/xtags/2018-06-23/530e8bf833c545e3a4a5e86cc97f5f79/000000533977.jpg",
		...]
	interest:"Things"
	marked:false
	name:"20180623-003物品标注"
	path:"C:/xtags/package/2018-06-23/530e8bf833c545e3a4a5e86cc97f5f79"
	requestor_id:"e86d3c79fc7545c2be74084351f9992a"
	type:"Classification"*/
	
	$.ajax({
		async: false,
	    type: "GET",
	    cache:true, 
	    dataType: 'json',
	    url: "/xtags/findOneVO",
	    timeout: 3000,
	    contentType: "application/json;utf-8",
	    success: function(msg) {
//	    	console.log(msg);
	    	currentURL = msg.imagesURL[0];
//	    	console.log(msg.imagesURL[0]);
	    	var imgHTML = "";
//	    	var eachWidth = ($(window).width() - 20 ) / msg.imagesURL.length;
//	    	console.log($(window).width() + "/" + eachWidth + "/" + msg.imagesURL.length);
	    	for (var i = 0; i < msg.imagesURL.length; i++) {
//	    		console.log(msg[i]);
				var one = '<div style="height:100px;margin:5px"><img height="100%" alt="image" name="eachimage" src="'+ msg.imagesURL[i] +'"><br></div>';
				imgHTML += one;
			}
	    	$("#imageurls").html(imgHTML);
	    	$("#name").html(msg.name);
	    	$("#description").html(msg.description);
	    	$("#interest").html(msg.interest);
	    	$("#tagtype").html(msg.type);
	    }
	});
	
	/**
	 * 获取图片真实长宽
	 */
	function getImgNaturalDimensions(oImg, callback) {
		var nWidth, nHeight;
		if (!oImg.naturalWidth) { // 现代浏览器
			nWidth = oImg.naturalWidth;
			nHeight = oImg.naturalHeight;
			console.log(nHeight);
			callback({w: nWidth, h:nHeight});
		}
	}
	
	function getImageWidth(url,callback){
		var img = new Image();
		img.src = url;
		
		// 如果图片被缓存，则直接返回缓存数据
		if(img.complete){
		    callback(img.width, img.height);
		}else{
	            // 完全加载完毕的事件
		    img.onload = function(){
			callback(img.width, img.height);
		    }
	        }
		
	}
	
	/**
	 * 绘制展示的图片
	 */
	function drawImage(){
		var cannvas = $("#canvas");
		if(canvas.getContext){  
			//获取对应的CanvasRenderingContext2D对象(画笔)
			var ctx = canvas.getContext("2d");
			//创建新的图片对象
			var img = new Image();
			//指定图片的URL
			img.src = currentURL;
			
			
			var image1 = $('img[name="eachimage"]')[0];
//			console.log(image1);
			console.log(image1.width +","+ image1.height);
			
			getImgNaturalDimensions(image1, function(dimensions){
				var naturalW = dimensions.w;
				var naturalH = dimensions.h;
//				console.log(dimensions.w);
			});
			
			/*var displayW = 0;
			var displayH = 0;
			var rate = 0.0;
			var imageli = $('#imageurls').children('li:eq('+ currentindex +')');
			console.log(imageli);
			getImgNaturalDimensions(img, function(dimensions){
				var naturalW = dimensions.w;
				var naturalH = dimensions.h;
				console.log(dimensions.w);
				rate = (naturalH >= naturalW ) ? (naturalH / 800) : (naturalW / 800);//精度...
				displayW = naturalW / rate;
				displayH = naturalH / rate;
			});*/
			/*var img = document.getElementById("oImg");
			getImgNaturalDimensions(img, function(dimensions){
				console.log(dimensions.w);
			});*/
			
//			var naturalWidth = img.naturalWidth
			//浏览器加载图片完毕后再绘制图片
			img.onload = function(){
				//以Canvas画布上的坐标(0,0)为起始点，绘制图像
				ctx.drawImage(img, 0, 0);
//				ctx.drawImage(img, 0, 0, displayW, displayH);             
			};
		}
	}
	
	drawImage();//canvas绘制图片
	/**
	 * 分类标注操作函数
	 */
	function markClassify(){
		
	}
	/**
	 * 标框标注操作函数
	 */
	function markFrame(){
		
	}
	/**
	 * 边界标注操作函数
	 */
	function markBorder(){
		
	}
	/**
	 * 判断当前标注类型进行标注
	 */
	function markCurrent(){
		
	}
	
	
	
});