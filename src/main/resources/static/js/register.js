/**
 * Created by st0001 on 2017/12/13.
 */
$().ready(function() {
	var validate = null;
	
    $("#register_form").validate({
        rules: {
        	username: {
                required: true,
                minlength: 5,
            	maxlength: 32
            },
            useremail:{
            	required: true,
            	email:true
            },
            userpassword: {
                required: true,
                minlength: 5,
            	maxlength: 32
            },
            confirmpassword: {
                required: true,
                equalTo: "#userpassword"
            },
            usercode: "required"
        },
        messages: {
        	username: {
                required: "请输入用户名",
                minlength: "不少于{0}个字符",
                maxlength: "不大于{0}个字符"
            },
            useremail:{
            	required: "请输入邮箱",
            	email: "请检查邮箱是否有效"
            },
            userpassword: {
                required: "请输入密码",
                minlength: "密码不能小于{0}个字符",
                maxlength: "密码不能超过{0}个字符"
            },
            confirmpassword: {
                required: "请再次输入密码",
                equalTo: "两次密码不一样"
            },
            usercode: "请输入验证码"
        }
    });
    
    
    $("#getCode").click(function(){
		var useremail = $("#useremail").val();
		console.log(useremail);
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "/hostel/sendMsg",
			data : JSON.stringify({
				"email" : useremail
			}),
			timeout: 5000,
			dataType : "json",
			success : function(data) {
				console.log(data);
				//发送成功后禁用发送按钮
				if (data.result=="success") {
					validate = data;
					toastr.success("请查看邮箱验证码");
					$("#getCode").attr("disabled",true);
				}else if (data.result=="used") {
					toastr.warning("邮箱已占用");
				}else {
					toastr.warning("发送失败，请刷新重试");
				}
			},error: function(error){
				console.log(error);
				toastr.warning("邮件可能被拦截或服务器超时，请刷新重试！");
			}
		});
	});
    
    
    $("#register").click(function(){
    	var valid = $("#register_form").valid();
    	if(!valid){
    		toastr.warning("检查输入！");
    	}else{
    		var username = $("#username").val();
        	var userpassword = $("#userpassword").val();
        	var useremail = $("#useremail").val();
        	var usercode = $("#usercode").val();
        	var type=$(':radio:checked').val();
//        	console.log(username + "/" + userpassword + "/" + useremail + "/" + type);
        	$.ajax({
        		type:"POST",
        		contentType:'application/json',
        		//验证url
        		url: "/hostel/validateCode",
        		data:JSON.stringify({
        			//验证码验证模块
        			"hash":validate.hash,
        			"time":validate.time,
        			"code":usercode
        		}),
        		dataType:"json",
        		timeout:5000,
        		error: function(error){
        			msg = error.responseJSON.message;
        			console.log(msg);
        			toastr.error("验证失败！");
        		},
        		success: function(msg){
        			console.log(msg);
//        			var data = JSON.parse(msg);
                    var status = msg.result;
                    if (status == "error") {
        				toastr.error("验证码不正确");
        			}else if (status == "overdue") {
        				toastr.error("验证超时，请刷新重试");
        			}else if (status == "success") {
        				//验证成功后
    					$.ajax({
    						type:"POST",
    						contentType:'application/json',
    						url: "/hostel/signup",
    						data:JSON.stringify({
    							"username":username,
    							"password":userpassword,
    							"email":useremail,
    							"type":type
    						}),
    						dataType:"json",
    						timeout:15000,
    						success: function(msg){
    							console.log(msg);
    				            var status = msg.result;
    				            if (status == "error") {
    								toastr.error("注册失败！");
    								
    							}else if (status == "success") {
    								toastr.success("注册成功！");
    								setTimeout(function(){
    									window.location = "/hostel/login";
    								},2000);
    							}
    						},
    						error:function(){
    							console.log("回调失败！");
    						}
    					});
        				
        			}
        		} 
        	});
    	}
    	
    });
    
});


