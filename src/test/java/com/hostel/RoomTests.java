package com.hostel;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hostel.entity.Room;
import com.hostel.service.RoomService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomTests {
	
	@Autowired
	private RoomService roomService;
	
	private String[] NO = {
		"101",
		"102",
		"103",
		"104",
		"105",
		"108",
		"202",
		"204",
		"205",
		"208"
	};
	//Single/Double/Triple/Size/Suite/Bed
	private String[] type = {
		"Single",
		"Single",
		"Double",
		"Double",
		"Double",
		"Triple",
		"Size",
		"Size",
		"Size",
		"Suite"
	};
	
	private double[] price = {
		299,
		299,
		399,
		399,
		399,
		499,
		359,
		359,
		359,
		699
	};
	
	private String[] description = {
		"单人间；网络；早餐",
		"单人间；网络；早餐",
		"双人间；网络；早餐",
		"双人间；网络；早餐",
		"双人间；网络；早餐",
		"三人间；网络；早餐",
		"大床房；网络；早餐",
		"大床房；网络；早餐",
		"大床房；网络；早餐",
		"套房；网络；早餐；接送"
	};
	
	@Test
	public void test() {
		for (int i = 0; i < 10; i++) {
			Room room = new Room(
					UUID.randomUUID().toString().replaceAll("-", ""),
					NO[i] ,
					"f3febb05f49a4bc1a10fe1b3cc36e3ba",
					type[i],
					price[i],
					description[i]);
			System.out.println(roomService.addRoom(room));
		}
	}

}
