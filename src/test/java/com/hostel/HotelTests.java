package com.hostel;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hostel.entity.Hotel;
import com.hostel.entity.Page;
import com.hostel.service.HotelService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HotelTests {
	
	
	private String[] name = {
			"流年青年客栈",
			"南京夫子庙国际青年旅舍",
			"南京瞻园国际青年旅舍",
			
			"海风之谷酒店",
			"国际青年会议酒店",
			"金陵饭店",
			"南京香格里拉大酒店",
			"南京绿地洲际酒店",
			
			"花迹酒店",
			"颐和公馆",
	};
	
	private String[] address = {
		"南京秦淮区石鼓路193号",
		"南京市秦淮区夫子庙平江府路68号",
		"南京市夫子庙大石坝街142号",
		
		"深圳市大朋新区南澳街道水头沙海滨路1号",
		"南京市建邺区邺城路8号",
		"南京市鼓楼区汉中路2号",
		"南京市鼓楼区中央路329号",
		"南京市鼓楼区中央路1号",
		
		"南京秦淮区老门东中营52号",
		"南京鼓楼区江苏路3号"
		
	};
	private String[] description = {
			"流年青年客栈（南京新街口店）位于石鼓路，秦淮区地铁二号线上海路站3号出口。大厅面积400平方米，有阅读区，酒吧区，娱乐区。",
			"南京最早的青年旅舍，位于夫子庙的街口，地处秦淮河畔，拥有繁华都市与江南水乡的碰撞感。",
			"国际青年旅舍，正对面是王导谢安的乌衣巷，临近李香君故居。地理位置极佳，每日清晨听到的是秦淮河畔的鸟语花香。",
			
			"海风之谷酒店依山面海而建，环境优美，空气新鲜。是休闲度假、朋友同学聚会、公司party、亲子出游的最佳选择。酒店附近有：海滨浴场、观海烧烤。",
			"国际青年会议酒店位于南京，提供免费WiFi，距离雨花台风景区有不到20分钟的路程，距离南京大学有不到25分钟的路程。",
			"宏伟的金陵饭店于1983年开业，位于南京市中心的新街口广场，地处高档的环境，距离新街口地铁站（1号线和2号线）仅200米，提供1000间带免费互联网的客房、1个室内温水游泳池以及6个餐饮场所。",
			"南京香格里拉大酒店坐落在繁华的鼓楼区的中央地段。南京香格里拉大酒店提供便利的地理位置、完善的会议设施和专业的服务人员，为商务旅客和休闲旅客提供了绝佳的下榻之地。",
			"南京绿地洲际酒店坐落在鼓楼广场的中心地带，距离鼓楼地铁站（1号线和4号线）的4A出口有1分钟步行路程，提供433间位于49楼以上的现代化独特客房。客人可以在客房内欣赏到南京市天际线的景致。",
			
			"花迹酒店隐藏在老门东明清建筑与民国建筑的老宅群内，离夫子庙不远。这是一座城南明清时期传统的三进式老宅院。",
			"颐和公馆掩映在大理石树小巷中，坐落在南京历史织物和遗产保护计划区中，周围洋溢着传统氛围，享有经典的民国建筑风格。"
	};
	private String[] type = {
			"Inn",
			"Inn",
			"Inn",
			
			"Hotel",
			"Hotel",
			"Hotel",
			"Hotel",
			"Hotel",
			
			"Homestay",
			"Homestay"
	};
	
	private String[] image = {
			"image/timg (4).jpg;image/timg (3).jpg;image/timg (5).jpg",
			"image/timg (5).jpg;image/timg (4).jpg;image/timg (13).jpg",
			"image/timg (6).jpg;image/timg (8).jpg;image/timg (11).jpg",
			
			"image/timg (18).jpg;image/timg (21).jpg;image/timg (22).jpg",
			"image/timg (15).jpg;image/timg (16).jpg;image/timg (9).jpg",
			"image/timg (16).jpg;image/timg (9).jpg;image/timg (17).jpg",
			"image/timg (9).jpg;image/timg (16).jpg;image/timg (15).jpg",
			"image/timg (1).jpg;image/timg (3).jpg;image/timg (4).jpg",
			
			"image/timg (10).jpg;image/timg (13).jpg;image/timg (12).jpg",
			"image/timg (8).jpg;image/timg (9).jpg;image/timg (21).jpg"
	};
	
	@Autowired
	private HotelService hotelService;

	/*@Test
	public void test01() {
		Hotel hotel = new Hotel(
				UUID.randomUUID().toString().replaceAll("-", ""),
				"天空之城客栈",
				"",
				"客栈坐落在美丽的湖畔，碧水蓝天烟波浩渺，湖光山色美不胜收。",
				"025-8968021",
				"image/timg (1).jpg;image/timg (3).jpg;image/timg (4).jpg",
				"Inn",
				new Date());
		String result = hotelService.release(hotel);
		System.out.println(result);
	}*/
	
	@Test
	public void test02() {
		Page page = new Page(hotelService.count(), 1, 8);
		String all = hotelService.allProjects(page);
		System.out.println(all);
	}
	
	@Test
	public void test01() {
		for (int i = 0; i < 10; i++) {
			Hotel hotel = new Hotel(
					UUID.randomUUID().toString().replaceAll("-", ""), 
					name[i], 
					address[i], 
					description[i], 
					"025-8968000", 
					image[i], 
					type[i], 
					new Date());
			String result = hotelService.release(hotel);
			System.out.println(result);
		}
	}
	

}
