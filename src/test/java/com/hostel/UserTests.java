package com.hostel;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hostel.entity.User;
import com.hostel.service.UserService;

/**
 * 
 * @ClassName: UserTests  
 * @Description: 测试用户注册登陆 
 * @author Brian  
 * @date 2018年6月22日  
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTests {
	
	@Autowired
	private UserService userService;
	//注册
	@Test
	public void signup() {
		User worker = new User(UUID.randomUUID().toString().replaceAll("-", ""),
				"Bryan","ren_blue@foxmail.com", "nightfall", new Date());
		System.out.println(userService.userRegister(worker));
		
	}
	//登陆
	@Test
	public void login() {
		System.out.println(userService.userLogin("ren_blue@foxmail.com", "nightfall"));
	}
}
