package com.hostel;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hostel.tools.FileUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderTests {
	
	//测试读取图片
	@Value("${file.url}")
	private String fileurl;
	
	@Test
	public void getImagePath() throws FileNotFoundException {
	    String imageFolder = "C:/xtags/package/2018-06-23/81e0831ba2724514a3b3470fbcfdbbb2";
	    FileUtil.getImagesPath(imageFolder);
//	    System.out.println(fileurl);
	    ArrayList<String> urls = FileUtil.getImagesURL(imageFolder, fileurl);
	    for (int i = 0; i < urls.size(); i++) {
			System.out.println(urls.get(i));
		}
	}
	
	

}
